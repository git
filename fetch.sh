#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "fetch $i" 1>&2
  cd $i
  git fetch $*

  if [ $? -ne 0 ]; then
    echo "fetch FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
