#! /bin/sh

# Update all submodules to remote HEAD by running:
#
# git submodule update --remote
#
# Normally you would follow this with commit.
#

. etc/git/modules

wd=`pwd`

for i in $sub_modules; do
  echo "submodule update $i" 1>&2
  cd $i
  git submodule update --remote $*

  if [ $? -ne 0 ]; then
    echo "submodule update FAILED" 1>&2
    exit 1
  fi

  # In case our submodules have their own submodules.
  #
  git submodule foreach git submodule update --init --recursive

  if [ $? -ne 0 ]; then
    echo "submodule update FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
