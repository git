#! /bin/sh

. etc/git/modules

wd=`pwd`

if [ "$1" = "-i" ]; then
  add=n
else
  add=y
fi

if [ "$EDITOR" = "" ]; then
  echo "no editor specified with the EDITOR variable" 1>&2
  exit 1
fi

msg_file=`mktemp -p /tmp`
$EDITOR "$msg_file"

if [ $? -ne 0 ]; then
  echo "$EDITOR failed" 1>&2
  rm -f $msg_file
  exit 1
fi

if test ! -s "$msg_file"; then
  echo "commit message is empty" 1>&2
  rm -f $msg_file
  exit 1
fi

for i in $modules; do
  echo "commit $i" 1>&2
  cd $i
  if [ "$add" = "y" ]; then
    git add -A .

    if [ $? -ne 0 ]; then
      echo "add FAILED" 1>&2
      exit 1
    fi

  fi
  git commit -F $msg_file

#  if [ $? -ne 0 ]; then
#    echo "commit FAILED" 1>&2
#    exit 1
#  fi

  cd $wd
done

rm -f $msg_file
