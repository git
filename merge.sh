#! /bin/sh

. etc/git/modules

wd=`pwd`

if [ "$1" = "" ]; then
  echo "source branch name expected" 1>&2
  exit 1
fi

for i in $modules; do
  echo "merge $i" 1>&2
  cd $i

  br=`git symbolic-ref -q HEAD`
  br=`echo $br | sed -e 's%^refs/heads/%%'`

  # Only allow fast-forward merges into master.
  #
  if [ "$br" = "master" ]; then
    git merge --ff-only $*
  else
    git merge $*
  fi

  if [ $? -ne 0 ]; then
    echo                1>&2
    echo "merge FAILED" 1>&2
    echo                1>&2

    # Merge failures (conflicts) into non-master branches are ok.
    #
    if [ "$br" = "master" ]; then
      exit 1
    fi
  fi

  cd $wd
done
