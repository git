#! /bin/sh

. etc/git/modules

wd=`pwd`

if [ "$1" = "" ]; then
  echo "source branch name expected" 1>&2
  exit 1
fi

for i in $modules; do
  echo "rebase $i" 1>&2
  cd $i
  git rebase $*

  if [ $? -ne 0 ]; then
    echo "rebase FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
