#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "branch $i" 1>&2
  cd $i
  git branch $*

  if [ $? -ne 0 ]; then
    echo "branch FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
