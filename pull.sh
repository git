#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "pull $i" 1>&2
  cd $i
  git pull $*

  if [ $? -ne 0 ]; then
    echo "pull FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
