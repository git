#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "remote $i" 1>&2
  cd $i
  git remote $*

  if [ $? -ne 0 ]; then
    echo "remote FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
