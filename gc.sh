#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "gc $i" 1>&2
  cd $i
  git gc

  if [ $? -ne 0 ]; then
    echo "gc FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
