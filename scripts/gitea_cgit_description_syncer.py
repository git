#!/usr/bin/python3
import requests
import sys
import os
from dotenv import load_dotenv
from pathlib2 import Path

if len(sys.argv) >= 2:
    owner = sys.argv[1]
    repo = sys.argv[2]
else:
    print("Specify the repository owner and name")
    sys.exit(1)

load_dotenv()
# GITEA SETTINGS
endpoint_gitea = os.environ.get('ENDPOINT_GITEA')
token_gitea = os.environ.get('TOKEN_GITEA')

# CGIT SETTINGS
local_path = os.environ.get('LOCAL_PATH')

if None in (endpoint_gitea, token_gitea, local_path):
    print("Could not load environment variables. Please check your .env file.")
    sys.exit(0)

URL = endpoint_gitea + '/api/v1/repos/' + owner + '/' + repo
try:
    response = requests.get(
    URL,
    headers = {'accept': 'application/json', 'Authorization': token_gitea},
    )
    status = response.status_code
    if status == 200:
        data = response.json()
        description_gitea = data['description']
        print(f"Description (Gitea): {description_gitea}")
    else:
        print('Error: ', status)
except KeyError:
    print("No or faulty description found in Gitea. ABORTING.")
    sys.exit(1)
except requests.exceptions.ConnectionError as err:
    print("Connection failed.")
    sys.exit(1)
except requests.exceptions.HTTPError as err:
    print(err)
    sys.exit(1)
file_path = local_path + '/' + repo + '.git/description'
file_content = Path(file_path).read_text()
description_local = file_content.replace('\n', '')
print(f"Description (Local): {description_local}")
if description_gitea == description_local:
    print("Matches, nothing to change.")
elif not description_gitea:
    print("Empty - Writing placeholder.")
    Path(file_path).write_text('Untitled / Work in Progress.')
else:
    print("Does not match, replacing local file.")
    Path(file_path).write_text(description_gitea)
