#! /bin/sh

. etc/git/modules

wd=`pwd`

if [ "$1" = "" ]; then
  echo "branch name expected" 1>&2
  exit 1
fi

for i in $modules; do
  echo "checkout $i" 1>&2
  cd $i
  git checkout $*

  if [ $? -ne 0 ]; then
    echo "checkout FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
