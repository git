#! /bin/sh

. etc/git/modules

wd=`pwd`

if [ "$1" = "" ]; then
  echo "missing version" 1>&2
  exit 1
fi

if [ "$tag_modules" = "" ]; then
  tag_modules="$modules"
fi

for i in $tag_modules; do
  echo "tag $i" 1>&2
  cd $i
  git tag -a "v$1" -m "Tag version $1"

  if [ $? -ne 0 ]; then
    echo "tag FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
