#!/bin/sh
docker run -d \
  -e DRONE_RPC_PROTO=https \
  -e DRONE_RPC_HOST=$RUNNERINTRANET \
  -e DRONE_RPC_SECRET=$RPCSEC \
  -e DRONE_RUNNER_NAME=$RUNNERHOST \
  -e DRONE_UI_PASSWORD=$RUNNERPASS \
  -e DRONE_UI_USERNAME=$RUNNERUSER \
  -e DRONE_UI_REALM=DroneRealm \
  -e DRONE_DEBUG=true \
  -p 127.0.0.3:3001:3000 \
  --dns 192.168.0.115 \
  --restart always \
  --name runner \
  -v /opt/drone/runner_data:/data \
  drone/drone-runner-ssh
