#!/bin/sh
docker run \
  --volume=/opt/drone/data:/data \
  --volume=$CERT:$CERT \
  --volume=$KEY:$KEY \
  --env=DRONE_GITEA_SERVER=https://git.com.de \
  --env=DRONE_GITEA_CLIENT_ID=$GITEAID \
  --env=DRONE_GITEA_CLIENT_SECRET=$GITEASEC \
  --env=DRONE_RPC_SECRET=$RPCSEC \
  --env=DRONE_SERVER_HOST=$DRONEINTRANET \
  --env=DRONE_SERVER_PROTO=https \
  --env=DRONE_DATABASE_DRIVER=postgres \
  --env=DRONE_DATABASE_DATASOURCE=postgres://$DBUSER:$DBPASS@$DBHOST:5432/$DB?sslmode=disable \
  --env=DRONE_DATABASE_SECRET=$DBSEC \
  --env=DRONE_PROMETHEUS_ANONYMOUS_ACCESS=true \
  --env=DRONE_TLS_CERT=$CERT \
  --env=DRONE_TLS_KEY=$KEY \
  --env=DRONE_USER_CREATE=username:drone,admin:true \
  --env=DRONE_DEBUG=true \
  --publish=127.0.0.3:80:80 \
  --publish=127.0.0.3:443:443 \
  --restart=always \
  --detach=true \
  --name=drone \
  drone/drone:2

# Disabled:
# --env=DRONE_YAML_ENDPOINT=http://172.17.0.3:3005 \
