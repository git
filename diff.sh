#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "diff $i" 1>&2
  cd $i

  git --no-pager diff $*

  if [ $? -ne 0 ]; then
    echo "diff FAILED" 1>&2
    exit 1
  fi

  git --no-pager diff --check

  if [ $? -ne 0 ]; then
    echo "diff --check FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
