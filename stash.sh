#! /bin/sh

. etc/git/modules

wd=`pwd`

for i in $modules; do
  echo "stash $i" 1>&2
  cd $i

  if [ "$1" = "pop" -o "$1" = "apply" ]; then
    l=`git stash list`
    if [ -z "$l" ]; then
      cd $wd
      continue
    fi
  fi

  git stash $*

  if [ $? -ne 0 ]; then
    echo "stash FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
